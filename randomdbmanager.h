#ifndef RANDOMDBMANAGER_H
#define RANDOMDBMANAGER_H

#include "dbmanager.h"

class RandomDBManager : DBManager
{
public:
    RandomDBManager(const QString &path);
    ~RandomDBManager();

    bool CreateTable();
    bool AddData(QVector<QVector<QString>> &data);
    bool RemoveCertainData();
};

#endif // RANDOMDBMANAGER_H
