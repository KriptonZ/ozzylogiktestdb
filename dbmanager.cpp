#include "dbmanager.h"
#include <QSqlQuery>

DBManager::DBManager(const QString &path)
{
    DB = QSqlDatabase::addDatabase("QSQLITE");
    DB.setDatabaseName(path);
    if (!DB.open())
    {
        qDebug() << "Can't open/create " << path;
    }
}

DBManager::~DBManager()
{
    if (DB.isOpen())
    {
        DB.close();
    }
}

bool DBManager::ExecuteNonQuery(QString &query_string)
{
    QSqlQuery query;
    query.prepare(query_string);
    return query.exec();
}
