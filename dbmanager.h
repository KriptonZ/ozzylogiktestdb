#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QSqlDatabase>

class DBManager
{
public:
    DBManager(const QString &path);
    virtual ~DBManager();

    bool ExecuteNonQuery(QString &query);
protected:
    QSqlDatabase DB;
};

#endif // DBMANAGER_H
