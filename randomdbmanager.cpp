#include "randomdbmanager.h"
#include <QDebug>

RandomDBManager::RandomDBManager(const QString &path) : DBManager(path) {}

RandomDBManager::~RandomDBManager() {}

bool RandomDBManager::CreateTable()
{
    QString QueryString = "DROP TABLE IF EXISTS random_data;";
    if(!DBManager::ExecuteNonQuery(QueryString))
    {
        qDebug() << "Can't drop the table.";
        return false;
    }

    QueryString = "CREATE TABLE random_data(Column1 TEXT, "
                                                   "Column2 TEXT,"
                                                   "Column3 TEXT,"
                                                   "Column4 TEXT,"
                                                   "Column5 TEXT,"
                                                   "Column6 TEXT);";
    if(!DBManager::ExecuteNonQuery(QueryString))
    {
        qDebug() << "Can't create table.";
        return false;
    }

    return true;
}

bool RandomDBManager::AddData(QVector<QVector<QString>> &data)
{
    QString QueryString = "INSERT INTO random_data"
                          "(Column1, Column2, Column3, Column4, Column5, Column6) "
                          "VALUES ('";
    for (uint i = 0; i < data.size(); i++)
    {
        for (uint j = 0; j < data[0].size() - 1; j++)
        {
            QueryString += (data[i][j] + "', '");
        }
        QueryString += (data[i].back() + "'), ('");
    }
    QueryString.chop(4);
    QueryString += ';';

    if(!DBManager::ExecuteNonQuery(QueryString))
    {
        qDebug() << "Can't add data.";
        return false;
    }

    return true;
}

bool RandomDBManager::RemoveCertainData()
{
    QString QueryString = "DELETE FROM random_data WHERE Column2 GLOB '[0-9]*'";
    if(!DBManager::ExecuteNonQuery(QueryString))
    {
        qDebug() << "Can't remove data.";
        return false;
    }

    return true;
}
