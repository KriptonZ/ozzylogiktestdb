#include <QCoreApplication>
#include <fstream>
#include "randomdbmanager.h"

using namespace std;

bool CreateCSV(const string file_name, int columns_count, int field_length, int records_count);
bool EditCSV(const string input_file_name, const string output_file_name, QString remove_criteria, QString replace_criteria, char replacement_char);
bool CreateDB(const string input_file_name);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // 1.A Сгенерировать csv файл из 1024 записей по 6 столбцов, заполненных строками
    // случайных символов(цифры и латинские буквы) длиной по 8 символов.
    const string FILE_NAME = "db.csv";
    const int COLUMNS_AMOUNT = 6;
    int FieldLength = 8;
    int RecordsAmount = 1024;

    CreateCSV(FILE_NAME, COLUMNS_AMOUNT, FieldLength, RecordsAmount) ?
    qDebug() << "CSV file successfully created!\n" :
    qDebug() << "Failed to create CSV file!\n";

    // B.Считать содержимое файла,
    // заменить нечетные цифры символом #,
    // удалить записи, в которых любая из шести строк начинается с гласной буквы,
    // сохранить отредактированный файл с другим именем.
    const QString VOWELS = "AEIOUYaeioy";
    const QString UNEVEN = "13579";
    const string NEW_FILE_NAME = "db_edited.csv";
    char Replacement = '#';
    EditCSV(FILE_NAME, NEW_FILE_NAME, VOWELS, UNEVEN, Replacement) ?
    qDebug() << "CSV file successfully changed!\n" :
    qDebug() << "Failed to change CSV file!\n";

    // C. Считать содержимое файла из пункта А,
    // создать программно базу данных sqlite,
    // сохранить все данные в таблицу.
    // Средствами sql удалить записи, в которых во втором столбце первый символ цифра.
    CreateDB(FILE_NAME) ?
    qDebug() << "Database successfully created!\n" :
    qDebug() << "Failed to create database!\n";

    return a.exec();
}

char GetRandomChar() // Returns random digit or English alphabet charater
{
    // There are 62 required charcters (10 digits + 26 upper case letters + 26 lower case letters).
    // In ASCII table digits start with code 46
    int CharCode = 48 + rand() % 62;

    // Since there are 7 extra characters between digits and upper case letters, we need additonal shift
    // 57 is '9'
    if (CharCode > 57)
    {
        CharCode += 7;
    }

    // Same for 6 extra characters between upper case letters and lower case letters
    // 90 is 'Z'
    if (CharCode > 90)
    {
        CharCode += 6;
    }

    return (char)CharCode;
}

bool WriteToCSV(const string file_name, const string& data)
{
    ofstream DBFileStream;
    DBFileStream.open(file_name);
    if (!DBFileStream.is_open())
    {
        return false;
    }
    DBFileStream << data;
    DBFileStream.close();

    return true;
}

bool ReadFromCSV(const string file_name, QVector<QVector<QString>>& InputData)
{
    ifstream DBFileStream;
    DBFileStream.open(file_name);
    if (!DBFileStream.is_open())
    {
        return false;
    }

    QVector<QString> Row;
    string ReadLine;
    while (DBFileStream >> ReadLine)
    {
        Row.clear();
        for (QString token : QString::fromStdString(ReadLine).split(','))
        {
            Row.push_back(token);
        }
        InputData.push_back(Row);
    }

    DBFileStream.close();
    return true;
}

bool CreateCSV(const string file_name, int columns_count, int field_length, int records_count)
{
    srand(time(NULL));
    string OutputLine, OutputData;

    for (int i = 0; i < records_count; i++)
    {
        OutputLine.clear();
        for (int j = 0; j < columns_count; j++)
        {
            for (int k = 0; k < field_length; k++)
            {
                OutputLine += GetRandomChar();
            }
            OutputLine += ',';
        }
        OutputLine[OutputLine.length() - 1] = '\n';
        OutputData += OutputLine;
    }
    return WriteToCSV(file_name, OutputData);
}

bool EditCSV(const string input_file_name, const string output_file_name, QString remove_criteria, QString replace_criteria, char replacement_char)
{
    QVector<QVector<QString>> InputData;
    if (!ReadFromCSV(input_file_name, InputData))
    {
        return false;
    }

    QString OutputData, OutputLine;
    bool is_exclude;

    for (uint i = 0; i < InputData.size(); i++)
    {
        is_exclude = false;
        OutputLine.clear();
        for (uint j = 0; j < InputData[0].size(); j++)
        {
            // Check if we need to exclude the line
            if (remove_criteria.contains(InputData[i][j][0]))
            {
                is_exclude = true;
                break;
            }

            // Replace the characters
            for (QChar& c : InputData[i][j])
            {
                if (replace_criteria.contains(c))
                {
                    c = replacement_char;
                }
            }
            OutputLine += (InputData[i][j] + ',');
        }

        if (!is_exclude)
        {
            OutputLine[OutputLine.length() - 1] = '\n';
            OutputData += OutputLine;
        }
    }
    return WriteToCSV(output_file_name, OutputData.toStdString());
}

bool CreateDB(const string input_file_name)
{
    QVector<QVector<QString>> InputData;
    if (!ReadFromCSV(input_file_name, InputData))
    {
        return false;
    }

    RandomDBManager r_DB("RandomData.db");
    if (!r_DB.CreateTable())
    {
        return false;
    }

    if (!r_DB.AddData(InputData))
    {
        return false;
    }

    if (!r_DB.RemoveCertainData())
    {
        return false;
    }

    return true;
}
